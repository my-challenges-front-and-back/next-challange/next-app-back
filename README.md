# Backend challenge

The challenge is to create a crud where there is registration, editing, deletion, customer listing.

## What is inside?

This project uses lot of stuff as:

- [TypeScript](https://www.typescriptlang.org/)
- [Jest](https://jestjs.io/pt-BR/)
- [Commitlint](https://commitlint.js.org/#/)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [Husky](https://typicode.github.io/husky/#/)
- [MongoDb](https://www.mongodb.com/)

## Install dependencies

```bash
npm install
# or
yarn install
```

<br/>
<br/>

## ❗❗❗To perform the step below you need docker and docker-compose installed and running on your machine.

<br/>
<br/>

## ⚙️⚙️ create an instance of mongodb with docker

```bash
  docker-compose up -d
```

<br/>

## Start the app

```bash
npm run dev
# or
yarn dev
```

<br/>
<br/>

## Api routes

## Add

```bash
POST: http://localhost:5050/api/add
Params: {
  name: string
  cpf: string
  birthday_date: Date
  sale: number
  limit: number
  zip_code: string
  logradouro: string
  uf: string
  number: number
  city: string
  district: string
  avatar: File
}

```

<br/>
<br/>

## Get Clients

```bash
GET: http://localhost:5050/api/list?page=1&limit=${seu limite de clientes}

```

<br/>
<br/>

## Delete Client

```bash
DELETE: http://localhost:5050/api/del/${id_cliente}

```

<br/>
<br/>

## List Single Client

```bash
GET: http://localhost:5050/api/list/${id_cliente}

```

<br/>
<br/>

## Update

```bash
PUT: http://localhost:5050/api/add
Params: {
  name: string
  cpf: string
  birthday_date: Date
  sale: number
  limit: number
  zip_code: string
  logradouro: string
  uf: string
  number: number
  city: string
  district: string
  avatar: File
}

```

import { Schema, model } from 'mongoose'

/* Client Interface */

interface ClientInterface {
  name: string
  cpf: string
  birthday_date: Date
  sale: number
  limit: number
  zip_code: string
  logradouro: string
  district: string
  number: number
  city: string
  uf: string
  avatar: string
}

/* Client Scheme */

const Client = new Schema<ClientInterface>({
  name: { type: String, required: [true, 'name cannot be empty'] },
  cpf: {
    type: String,
    required: [true, 'cpf cannot be empty'],
    validate: {
      validator: function (value: any) {
        return /[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}-?[0-9]{2}/.test(value)
      },
      message: () => `informe cpf válido`
    }
  },
  birthday_date: {
    type: Date,
    required: [true, 'birthday_data cannot be empty']
  },
  sale: { type: Number, required: [true, 'sale cannot be empty'] },
  limit: { type: Number, required: [true, 'limit cannot be empty'] },
  zip_code: {
    type: String,
    required: [true, 'zipcode cannot be empty'],
    validate: {
      validator: function (code: any) {
        return /[0-9]{5}-?[\d]{3}/.test(code)
      },
      message: 'informe um cep válido'
    }
  },
  logradouro: { type: String, required: [true, 'logradouro cannot be empty'] },
  district: { type: String, required: [true, 'district cannot be empty'] },
  number: { type: Number, required: [true, 'number cannot be empty'] },
  city: { type: String, required: [true, 'city cannot be empty'] },
  uf: { type: String, required: [true, 'uf cannot be empty'] },
  avatar: { type: String }
})

/* Client Model */
export const ClientModel = model<ClientInterface>('Client', Client)

import { Request, Response } from 'express'
import { ClientModel } from '../models/client'

export default async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    if (!id) {
      res.status(400).json({ error: 'provide a valid id' })
    }
    await ClientModel.deleteOne({ _id: id })
    res.status(200).json({ sucess: true })
  } catch (error) {
    res.json(error)
  }
}

import { Request, Response } from 'express'
import { ClientModel } from '../models/client'

export default async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    if (!id) {
      return res.status(400).json({ error: 'provide a valid id' })
    }
    const client = await ClientModel.find({ _id: id })
    return res.status(200).json(client)
  } catch (error) {
    return res.status(500).json(error)
  }
}

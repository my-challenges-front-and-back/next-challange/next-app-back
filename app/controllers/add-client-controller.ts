import { Request, Response } from 'express'
import { ErrorFields } from '../helpers/input-define-message'
import { ClientModel } from '../models/client'

export default async (req: Request, res: Response) => {
  try {
    const clientExists = await ClientModel.find({ cpf: req.body.cpf })
    if (clientExists.length !== 0) {
      return res.status(400).json({ error: 'cliente já cadastrado' })
    }
    const client = await ClientModel.create({
      ...req.body,
      avatar: req.file.fieldname
    })
    const addClient = await client.save()
    return res.status(201).json(addClient)
  } catch (error: any) {
    return error.name
      ? res.status(400).json({ error: ErrorFields(error.errors) })
      : res.status(500).json({ error: 'internal server error' })
  }
}

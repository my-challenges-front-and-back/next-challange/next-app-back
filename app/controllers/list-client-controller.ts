import { Request, Response } from 'express'
import { ClientModel } from '../models/client'

export default async (req: Request, res: Response) => {
  try {
    const page =
      parseInt(req.query.page as string) === 0
        ? 0
        : parseInt(req.query.page as string)
    const limit = parseInt(req.query.offset as string) || 3
    const countClients = await ClientModel.find({}).count()
    const clients = await ClientModel.find().skip(page).limit(limit)
    return res.status(200).json([[...clients], { total: countClients }])
  } catch (error) {
    return res.status(500).json(error)
  }
}

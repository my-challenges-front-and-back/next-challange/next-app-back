import { Request, Response } from 'express'
import { ClientModel } from '../models/client'

export default async (req: Request, res: Response) => {
  try {
    const { id } = req.params

    const client = await ClientModel.find({ _id: id })
    if (client.length === 0) {
      return res.status(400).json({ error: 'cliente não encontrado' })
    }

    await ClientModel.updateOne(
      { _id: id },
      {
        ...req.body,
        avatar: req.file?.fieldname || client[0].avatar
      }
    )
    return res.status(200).json({ success: true })
  } catch (error) {
    return res.status(500).json(error)
  }
}

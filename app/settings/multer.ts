import multer from 'multer'
import crypto from 'crypto'
import path from 'path'

const configBucket = {
  local: multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, path.resolve(__dirname, '..', 'tmp', 'figure'))
    },
    filename: (req, file, cb) => {
      crypto.randomBytes(16, (err, hash) => {
        file.fieldname = `${hash.toString('hex')}-${file.originalname}` // eslint-disable-line
        cb(null, file.fieldname)
      })
    }
  })
}

export default {
  dest: path.resolve(__dirname, '..', 'tmp', 'profile'),
  storage: configBucket.local,
  limits: {
    fileSize: 2 * 1024 * 1024
  },
  fileFilter: (req: any, file: any, cb: any) => {
    const allowedMimes = ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gig']
    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true)
    } else {
      cb(new Error('invalid file type.'))
    }
  }
}

import express from 'express'
import setupRoutes from './router'
import middlewares from './middlewares'
import path from 'path'

const app = express()
app.use(
  '/profile',
  express.static(path.resolve(__dirname, '..', 'tmp', 'figure'))
)
middlewares(app)
setupRoutes(app)

export default app

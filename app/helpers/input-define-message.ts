export const ErrorFields = (errors: any) => {
  let message = ''
  if (errors !== undefined) {
    Object.keys(errors).map((error, index) => {
      if (index === 0) message = errors[error].message
    })
  }
  return message
}

import 'dotenv/config'
import app from './settings/app'
import mongoConnect from './db/connect'

mongoConnect()
  .then(async () => {
    app.listen('5050', () =>
      console.log('aplication running in port 5050 🧨🧨🧨')
    )
  })
  .catch(console.error)

import { Router } from 'express'
import settingMulter from '../settings/multer'

import EditClientController from '../controllers/update-client-controller'
import multer from 'multer'

export default (router: Router) => {
  router.put(
    '/edit/:id',
    multer(settingMulter).single('avatar'),
    EditClientController
  )
}

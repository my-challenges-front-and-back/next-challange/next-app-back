import { Router } from 'express'
import DeleteClientController from '../controllers/delete-client-controller'

export default (router: Router) => {
  router.delete('/del/:id', DeleteClientController)
}

import { Router } from 'express'
import ListClientController from '../controllers/list-client-controller'

export default (router: Router) => {
  router.get('/list', ListClientController)
}

import { Router } from 'express'
import ListSingleClientController from '../controllers/list-single-client-controller'

export default (router: Router) => {
  router.get('/list/:id', ListSingleClientController)
}

import { Router } from 'express'
import multer from 'multer'
import AddClientController from '../controllers/add-client-controller'
import settingMulter from '../settings/multer'

export default (router: Router) => {
  router.post(
    '/add',
    multer(settingMulter).single('avatar'),
    AddClientController
  )
}

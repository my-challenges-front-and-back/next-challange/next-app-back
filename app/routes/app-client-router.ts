import { Router, Request, Response } from 'express'

export default (router: Router) => {
  router.get('/', (req: Request, res: Response) => {
    res.json({ app: 'running' })
  })
}
